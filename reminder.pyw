#!/usr/bin/env python3

import time
from win10toast import ToastNotifier

aviso = ToastNotifier()
aviso.show_toast("Corriendo ✅", "Aviso que me estoy ejecutando 😜👌🏽", duration=10)
contador = 0
while True:
    contador = contador + 1
    time.sleep(1200)  #Aquí colocas tu invervalo de tiempo en segundos. 20 min son 1200seg
    if(contador == 1):
        aviso.show_toast("Hey 😜", f"Llevas {contador} descanso ", duration=5)
    else:
        aviso.show_toast("Hey 😜", f"Llevas {contador} descansos", duration=5)
    aviso.show_toast("Holaaa  😎!!","Tómate un descanso de tu trabajo ❤ ",duration=10)


